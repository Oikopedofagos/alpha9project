# Installation

- Extract the contents of the compressed file "books.tar.gz"
- Build the executable file
- Run the executable file

```sh
$ tar -xf books.tar.gz
$ go build tcpServer.go
$ ./tcpServer
```

# Implementation

- The program starts a tcp server listening at localhost:9999
- Reads the names of the files of the directory "./books"
- Splits the files into N equal chunks and give each chunk to a different worker(default N=4)
- Each worker  build an index with key the word and value a struct containing {the frequency of this word in his chunk, the word and another index with key the name of a the file and value the frequence of this word in that file}
- After building the index the worker pushes the index into a channel. Workers monitor the channel and when the channel contains at least two indexes, a worker takes these two indexes and merge them into one with updated frequences for each word and updated index in which files and how many times per file this word appears. The resulting index is passed again into the channel
- After N-1 merges we have the final index containing all words appearing in the documents, their frequencies and in which files and how many times each word appears in the files
- Then the index is copied into an slice, since we need to sort the words based on the frequence. After passing the index into a slice and while still not sorted, again we break the slice into chunks giving them to different workers. The workers sort by frequence the names of the files
- Finally we sort the words based by their frequence in all files
- Our server is ready to respond to the command "common X" where X is a positive integer and "search word" where word is a given string. 

# Time Complexity
- N number of words
- n number of unique words
- m number of documents

Building the index require reading all words in all documents with complexity O(N)

Copying the index into slice has complexity O(n)

Sorting each word's slice has complexity O(m logm) and since we do it for all unique words O(nm logm)

Sorting the words based on their frequency has a complexity O(n logn)

The complexity to build the indexes and sort them is O(N+n(logn+mlogm))

The complexity to answer "Common X" is O(1)

The complexity to answer "Search" is O(1)
# Space Complexity

Space complexity for storing the n unique words O(n) and for storing the files that word appears O(m) for each word. 

Space complexity O(nm)

# Some Test Results

Based on the test data from the books,  the results I got with different number of Workers are the following

| Workers | Warm Up Time | Memory Allocated | Memory Used |
| ------ | ------ | ------ | ------ |
| 1 | 17s |1,2GB |847MB |
|2|12s|1,4GB|847MB|
|4|7,7s|1,5GB|847MB|
|8|7,4s|2,1GB|847MB|

Updated Version


| Workers | Warm Up Time | Memory Allocated | Memory Used |
| ------ | ------ | ------ | ------ |
| 1 | 17s |1,2GB |847MB |
|2|9,8s|1,2GB|847MB|
|4|6,8s|1,7GB|847MB|
|8|6,4s|2,3GB|847MB|
