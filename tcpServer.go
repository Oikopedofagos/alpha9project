package main

import (
	"./db"
	"bytes"
	"fmt"
	"net"
	"os"
	"strconv"
	"strings"
	"time"
)

func main() {
	port := "9999"
	if len(os.Args) == 2 {
		port = os.Args[1]
	}
	t1 := time.Now()
  data:=db.NewDB()
  data.BuildIndexes("./books")
	//index, words := db.BuildIndexes("./books")
	t2 := time.Now()
	dura := t2.Sub(t1)
	fmt.Println("Warm up down " + dura.String())
	l, err := net.Listen("tcp", ":"+port)
	if err != nil {
		fmt.Println("Error listening:", err.Error())
		os.Exit(1)
	}
	defer l.Close()
	fmt.Println("Listening on :" + port)
	for {
		conn, err := l.Accept()
		if err != nil {
			fmt.Println("Error accepting: ", err.Error())
			os.Exit(1)
		}

		fmt.Printf("Received message %s -> %s \n", conn.RemoteAddr(), conn.LocalAddr())

		go handleRequest(conn, data)
	}
}

func writeResults(conn net.Conn, results []db.InFile) {
	for _, r := range results {
		conn.Write([]byte(r.Filename + " " + strconv.Itoa(r.Frequence) + "\n"))
	}
}

// Handles incoming requests.
func handleRequest(conn net.Conn, data *db.DB) {
	defer conn.Close()
	buf := make([]byte, 1024)
	_, err := conn.Read(buf)
	if err != nil {
		fmt.Println("Error reading:", err.Error())
	}
	n := bytes.Index(buf, []byte{0})
	input := string(buf[:n-1])
	command := strings.Split(input, " ")
	if len(command) != 2 || (command[0] != "common" && command[0] != "search") {
		conn.Write([]byte("Invalid usage\n"))
	} else {
		if command[0] == "common" {
			param, err := strconv.ParseInt(command[1], 10, 32)
      fmt.Println(param)
			if err != nil || param < 0 {
				conn.Write([]byte("Give a Positive Integer\n"))
			} else {
				results := data.Common(param)
				writeResults(conn, results)
			}
		} else { // search command
			results := data.Search(command[1])
			writeResults(conn, results)
		}
	}
}
