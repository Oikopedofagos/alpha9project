package db

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"sort"
	"strings"
	"time"
	//"runtime/debug"
	"runtime"
	//"regexp"
)

type empty struct{}

type DB struct{
	index map[string]int
	sorted []*Word
}

func NewDB() *DB{
	d:=DB{}
	//d.BuildIndexes(directory)
	return &d
}

type Word struct {
	Frequence int
	InFiles   map[string]int
	Value     string
	Where     []*InFile
}

type InFile struct {
	Frequence int
	Filename  string
}

type byInFrequence []*InFile

func (k byInFrequence) Len() int           { return len(k) }
func (k byInFrequence) Swap(i, j int)      { k[i], k[j] = k[j], k[i] }
func (k byInFrequence) Less(i, j int) bool { return k[i].Frequence > k[j].Frequence }

type ByFrequence []*Word

func (a ByFrequence) Len() int           { return len(a) }
func (a ByFrequence) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }
func (a ByFrequence) Less(i, j int) bool { return a[i].Frequence > a[j].Frequence }

func sortWhere(words chan *Word, term chan bool, count chan empty) {

	for {
		select {
		case w := <- words:
			count<-empty{}
			l := 0
			//fmt.Println(len(w.Where))
			//fmt.Println(len(w.InFiles))
			for lo, co := range w.InFiles {
				in:=InFile{Filename: lo, Frequence:co}
				w.Where[l]=&in
				l++
			}
			sort.Sort(byInFrequence(w.Where))

		case <-term:
			return
		}
	}
}

func merge(idx1, idx2 map[string]*Word, st chan map[string]*Word) {
	var small, large map[string]*Word 
	if (len(idx1)<len(idx2)){
		small=idx1
		large=idx2
	}else{
		small=idx2
		large=idx1
	}
	for s, W2 := range small {
		W1, ok := large[s]
		if ok {
			getUpdatedWord(W1, W2)
			//delete(idx2, s)
		}else{
			large[s]=W2
		}
	}

	st <- large
}

func getUpdatedWord(w1, w2 *Word) {

	
	w1.Frequence +=  w2.Frequence
	for k, v := range w2.InFiles {
		w1.InFiles[k] = v
	}

}

/*func chunks(files []os.FileInfo, num int) [][]os.FileInfo {
	size := len(files)
	perChunk := size / num
	res := make([][]os.FileInfo, num)
	for i := 0; i < num-1; i++ {
		res[i] = files[i*perChunk : (i+1)*perChunk]
	}
	res[num-1] = files[(num-1)*perChunk:]
	return res
}*/

/*func whereChunks(arr []Word, num int) [][]Word {
	size := len(arr)
	perChunk := size / num
	res := make([][]Word, num)
	for i := 0; i < num-1; i++ {
		res[i] = arr[i*perChunk : (i+1)*perChunk]
	}
	res[num-1] = arr[(num-1)*perChunk:]
	return res
}*/

func builtPartialIndexes(directory string, files chan os.FileInfo, 
						 st chan map[string]*Word, term chan bool,count chan empty) {

	dict := make(map[string]*Word)

	for {
		select {
		case s := <- files:
			count<-empty{}
			content, err := ioutil.ReadFile(directory + "/" + s.Name())
			if err != nil {
				log.Fatal(err)
			}
			words := preprocess(string(content))

			process(words, s.Name(), dict)

		case <-term:
			st <- dict
			return
		}
	}
	//debug.FreeOSMemory()
	

}
/*func Counter(count chan empty,i int,exitc chan bool) {
	for j:=0;j<i;j++{
		<-count
	}
	exitc<-true
}*/
func (this *DB)BuildIndexes(directory string){

	files, err := ioutil.ReadDir(directory)
	if err != nil {
		log.Fatal(err)
	}

	t0 := time.Now()
	parallel := 8
	filesLen:=len(files)
	fileChan := make(chan os.FileInfo,filesLen)
	count:=make(chan empty, filesLen)
	//exitc:=make(chan bool, 1)
	terminate:=make(chan bool,parallel)
	st := make(chan map[string]*Word,parallel-1)



	for i := 0; i < parallel; i++ {
		go builtPartialIndexes(directory, fileChan, st, terminate,count)
	}

	for _,s := range files{
		fileChan<-s	
	}

	for i:=0;i<filesLen;i++{
		<-count
	}
	for i:=0; i < parallel;i++{
		terminate<-true
	}
	t1 := time.Now()
	fmt.Println("Build Time: " + t1.Sub(t0).String())

	//fmt.Println(runtime.NumGoroutine())
	for i := 0; i < parallel-1; i++ {
		//var idx1, idx2  map[string]Word
		idx1:= <- st
		idx2 := <- st
		go merge(idx1, idx2, st)
	}

	defer close(fileChan)
	defer close(count)
	defer close(terminate)
	
	//fmt.Println(runtime.NumGoroutine())
	//runtime.GC()
	//var dict map[string]Word
	dict  := <- st
	defer close(st)

	

	t2 := time.Now()
	fmt.Println("Merge Time: " + t2.Sub(t1).String())
	this.sorted = make([]*Word, len(dict))
	this.index = make(map[string]int)
	wordsLen:=len(dict)
	i := 0
	for v, k := range dict {
		k.Value = v
		k.Where = make([]*InFile, len(k.InFiles))
		this.sorted[i] = k
		i++
	}
	dict=nil
	wordChan:=make(chan *Word, wordsLen)
	wordCount:=make(chan empty, wordsLen)
	for i = 0; i < parallel; i++ {

		go sortWhere(wordChan, terminate, wordCount)
	}
	for _,w := range this.sorted{
		wordChan<-w	
	}
	sort.Sort(ByFrequence(this.sorted))
	for i = 0; i < len(this.sorted); i++ {
		param := this.sorted[i]
		this.index[param.Value] = i
	}

	for i=0; i < wordsLen;i++{
		<-wordCount
	}
	for i=0; i < parallel;i++{
		terminate<-true
	}
	defer close(wordChan)
	defer close(wordCount)
	//fmt.Println(runtime.NumGoroutine())
	//runtime.GC()
	
	t3 := time.Now()
	fmt.Println("Sort Time: " + t3.Sub(t2).String())
	//runtime.GC()
	mem:=&runtime.MemStats{}
	runtime.ReadMemStats(mem)
	fmt.Println("Memory acquired ", mem.Sys)
	fmt.Println("Memory used ", mem.Alloc)
	//debug.FreeOSMemory()
}

func (this *DB) Search(word string) []InFile {
	word=strings.ToLower(word)
	//sum := 0
	W, ok:=this.index[word]
	if ok{
		/*for 	_, k := range this.sorted[W].Where {
			sum += k.Frequence
		}
		fmt.Printf("sum %d \n", sum)*/
		res:=make([]InFile,len(this.sorted[W].Where))
		for i,r := range this.sorted[W].Where{
			res[i].Frequence = r.Frequence
			res[i].Filename = r.Filename
		}
		return res
	}
	return nil
}

func (this *DB) Common( num int64) []InFile {
	if num > int64(len(this.sorted)){ num = int64(len(this.sorted))}
	res := this.sorted[:num]
	ans := make([]InFile, num)
	for i, r := range res {
		ans[i].Frequence = r.Frequence
		ans[i].Filename = r.Value
	}

	return ans
}

func preprocess(data string) []string {
	/* commented since slow in golang
	re := regexp.MustCompile(`[.,\/#!$%\^&\*;:{}=\-_~()]|\d+|\s`)
	data =re.ReplaceAllString(data, " ")
	re = regexp.MustCompile(`\s{2,}`)
	data =re.ReplaceAllString(data, " ")
	*/
	data=strings.ToLower(data)
	
	repl:=strings.NewReplacer("\r", " ", "\n", " ", "\t", " ",
				 "0", " ", "1", " ", "2", " ", "3", " ",
				 "4", " ", "5", " ", "6", " ", "7", " ",
				 "8", " ", "9", " ", ".", " ", ",", " ",
				 "!", " ", "?", " ", ":", " ", "]", " ",
				 "[", " ", "(", " ", ")", " ", ";", " ",
				  "\\", " ", " /", " ", ">", " ", "<", " ",
				  "`", " ", "$", " ", "*", " ", "&", " ",
				  "%", " ", "@", " ", "-", " ", "_", " ",
			 	"+", " ", "~", " ", "|", " ", "'", " ", 
			 	 "\"", " ", "^", " ", "{", " ", "}", " ")

	data=repl.Replace(data)
	for strings.Contains(data, "  ") {
		data = strings.Replace(data, "  ", " ", -1)
	}
	
	words := strings.Split(data, " ")
	if len(words)>0 && words[len(words)-1]==""{
		return words[:len(words)-1]
	}
	return words
}

func process(words []string, filename string, dict map[string]*Word) {

	for i := 0; i < len(words); i++ {
		w, ok := dict[words[i]]

		if !ok {
			W := Word{Frequence: 1}
			W.InFiles = make(map[string]int)
			W.InFiles[filename] = 1
			dict[words[i]] = &W
		} else {
			w.Frequence += 1
			_, inok := w.InFiles[filename]
			if !inok {
				w.InFiles[filename] = 1
			} else {
				w.InFiles[filename] += 1
			}
		}
	}
}